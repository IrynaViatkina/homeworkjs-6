const user = createNewUser();

function createNewUser(){
	let user = {};
	user.fullName = prompt("Please, enter your first and last names: ");
		
	Object.defineProperty(user, 'firstName', {
		set: function(value){
			this.fullName = value + " " + this.fullName.slice(this.fullName.indexOf(' ') + 1);
		},
		get: function(){
			return this.fullName.slice(0, this.fullName.indexOf(' '));
		}
	});
	Object.defineProperty(user, 'lastName', {
		set: function(value){
			this.fullName = this.fullName.slice(0, this.fullName.indexOf(' ')) + " " + value;
		},
		get: function(){
			return this.fullName.slice(this.fullName.indexOf(' ') + 1);
		}
	});
	
	user.getLogin = function(){
		return (this.fullName.slice(0, 1) + this.fullName.slice(this.fullName.indexOf(' ') + 1)).toLowerCase();
	}
	return user;
}